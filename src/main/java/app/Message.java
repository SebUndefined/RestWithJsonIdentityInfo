/**
 * 
 */
package app;
import java.util.ArrayList;
import java.util.List;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * @author sebby
 *
 */
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@messageId")
public class Message {

	private Long id;
	private String message;
	//@JsonBackReference
	private List<Greeting> greetings;
	
	
	public Message(Long id, String message) {
		super();
		this.id = id;
		this.message = message;
		this.greetings = new ArrayList<>();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Greeting> getGreetings() {
		return greetings;
	}
	public void setGreetings(List<Greeting> greetings) {
		this.greetings = greetings;
	}
	
	public void addGreeting (Greeting greeting) {
		this.greetings.add(greeting);
	}
	
	
}
