package app;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author sebby
 *
 */
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@greetingId")
public class Greeting {

    private long id;
    private String content;
    //@JsonManagedReference
    private Message message;

    public Greeting(long id, String content, Message message) {
        this.id = id;
        this.content = content;
        this.message = message;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

	public Message getMessage() {
		return message;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
    
}
