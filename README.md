# RestWithJsonIdentityInfo

Simple application for showing the difference between usage of @JsonManagedReference (with JsonBackReference) and @JsonIdentityInfo.

For testing it, just download the project and open it in Eclipse. The program work with @JsonIdentityInfo annotation by default. 

For testing the @JsonManagedReference (with JsonBackReference) annotation, comment @JsonIdentityInfo and uncomment @JsonManagedReference and JsonBackReference in Greeting and Message classes. 

SebUndefined